import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:image_picker/image_picker.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:share/share.dart';

class Camera extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SOU FOTO OLA',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: UploadingImageToFirebaseStorage(),
    );
  }
}

final Color yellow = Color(0xfffbc31b);
final Color orange = Color(0xfffb6900);

class UploadingImageToFirebaseStorage extends StatefulWidget {
  @override
  _UploadingImageToFirebaseStorageState createState() =>
      _UploadingImageToFirebaseStorageState();
}

class _UploadingImageToFirebaseStorageState
    extends State<UploadingImageToFirebaseStorage> {
  //FILE DA IMAGEM FOTO
  File _imageFile;
  final picker = ImagePicker();
  String uploadedFileURL;
  List<String> attachments = [];
  //string rede social
  String redesocial =
      "https://instagram.com/supermercadocometa?igshid=1trr88eqnh54o";
  bool isHTML = false;
  String subject = "";
  String text = "";

  //String URL firebase Storage

  Future pickImage() async {
    final pickedFile = await picker.getImage(
      source: ImageSource.camera,
      imageQuality: 60,
    );
    setState(() {
      _imageFile = File(pickedFile.path);
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => shareImage(context)),
    );
  }

  final _recipientController = TextEditingController(
    text: 'design_800@hotmail.com',
  );

  final _subjectController = TextEditingController(text: 'cometa rede social');

  //envio para o firebase
  Future<void> uploadImageToFirebase(BuildContext context) async {
//Random e pra Diminuir tamanho do numero da image Firebase Storage
    int randomNumber = Random().nextInt(100000);
    String imageLocation = 'avantar/image${randomNumber}.jpg';

//URL GET Image Firebase Storage
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(imageLocation);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
    var downUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    uploadedFileURL = downUrl.toString();
    print(uploadedFileURL);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => qrcode(context)),
    );
    Timer(Duration(seconds: 2), () async {
      final Email email = Email(
        body: uploadedFileURL,
        subject: _subjectController.text,
        recipients: [_recipientController.text],
        attachmentPaths: attachments,
        isHTML: isHTML,
      );
      await FlutterEmailSender.send(email);
    });
    Timer(Duration(seconds: 13), () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => sigaRedesocial(context)),
      );
    });
  }

  Future<void> redeSocial(String path) async {
    text.isEmpty
        ? null
        : () {
            final RenderBox box = context.findRenderObject();
            Share.share(text,
                subject: subject,
                sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
          };
  }

//Rosto da temi PAGE1
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.only(top: 300),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  onPressed: () => pickImage(),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Text(
                    "Clique no botão para tirar a foto",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

//clique no botao para tirar a foto PAGE2
  Widget shareImage(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text(
          'Gostou da sua foto? Posso publicar nas nossas redes sociais? Se quiser pode tirar outra!',
          style: TextStyle(
            fontSize: 30,
          ),
        )),
        body: Center(
            child: Container(
                width: 700,
                height: 700,
                margin: const EdgeInsets.only(top: 8),
                child: Row(children: <Widget>[
                  Column(
                    children: [
                      Container(
                        width: 480,
                        height: 400,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(30.0),
                          child: _imageFile != null
                              ? Image.file(_imageFile)
                              : FlatButton(),
                        ),
                      ),
                      Container(
                        width: 480,
                        child: RaisedButton(
                          child: Text("SAIR",
                              style: TextStyle(
                                fontSize: 20,
                              )),
                        ),
                      ),
                      Container(
                        width: 480,
                        child: RaisedButton(
                          child: Text("Nova foto",
                              style: TextStyle(
                                fontSize: 20,
                              )),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 70, left: 40),
                        child: Text("@cometasupermercados",
                            style: TextStyle(
                              fontSize: 16,
                            )),
                      ),
                      Container(
                        width: 180,
                        margin: const EdgeInsets.only(left: 40),
                        child: RaisedButton(
                          child: Text("SIM",
                              style: TextStyle(
                                fontSize: 20,
                              )),
                          onPressed: () => uploadImageToFirebase(context),
                        ),
                      ),
                      Container(
                        width: 180,
                        margin: const EdgeInsets.only(left: 40),
                        child: RaisedButton(
                          child: Text("NAO",
                              style: TextStyle(
                                fontSize: 20,
                              )),
                        ),
                      )
                    ],
                  )
                ]))));
  }
// gostou da sua foto? PAGE3

//buttom da pagina
  Widget uploadImageButton(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 16.0),
            margin: const EdgeInsets.only(
                top: 0, left: 0.0, right: 3.0, bottom: 20.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [yellow, orange],
                ),
                borderRadius: BorderRadius.circular(30.0)),
            child: FlatButton(
              //Buttom FUTURE upload da imagem
              onPressed: () => uploadImageToFirebase(context),
              child: Text(
                "SIM",
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
            margin: const EdgeInsets.only(
                top: 0, left: 0.0, right: 0.0, bottom: 20.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [yellow, orange],
                ),
                borderRadius: BorderRadius.circular(30.0)),
            child: FlatButton(
              //Buttom FUTURE upload da imagem
              onPressed: () {},
              child: Text(
                "NAO",
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
        ],
      ),
    );
  }

// baixe a foto para seu dispositivo PAGE 4
  Widget qrcode(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 200),
            child: Text(
              "Baixe a foto para seu dispositivo",
              style: TextStyle(fontSize: 40, color: Colors.black),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20),
            height: 460,
            child: QrImage(
              //String da URL
              data: uploadedFileURL,
              size: 420.0,
              gapless: true,
              errorCorrectionLevel: QrErrorCorrectLevel.Q,
              foregroundColor: Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  // Siga nossas rede social, PAGE5
  Widget sigaRedesocial(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text(
        'Siga nossas redes sociais, sempre temos novas promoções!',
        style: TextStyle(
          fontSize: 30,
        ),
      )),
      body: Center(
        child: Container(
          width: 600,
          margin: const EdgeInsets.only(top: 200),
          child: Row(
            children: <Widget>[
              Column(
                children: [
                  Container(
                    child: QrImage(
                      //string rede social
                      data: redesocial,
                      size: 360.0,
                      gapless: true,
                      errorCorrectionLevel: QrErrorCorrectLevel.Q,
                      foregroundColor: Colors.black,
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(top: 70),
                    child: Text("@cometasupermercados"),
                  ),
                  Container(
                    child: Text("/cometasupermercados"),
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 40),
                    width: 180,
                    child: RaisedButton(
                        child: Text("Voltar",
                            style: TextStyle(
                              fontSize: 20,
                            )),
                        onPressed: () => qrcode(context)),
                  ),
                  Container(
                    width: 180,
                    margin: const EdgeInsets.only(left: 40),
                    child: RaisedButton(
                        child: Text("Sair",
                            style: TextStyle(
                              fontSize: 20,
                            )),
                        onPressed: () {}),
                  ),
                  Container(
                    width: 180,
                    margin: const EdgeInsets.only(left: 40),
                    child: RaisedButton(
                      child: Text("Nova foto",
                          style: TextStyle(
                            fontSize: 20,
                          )),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}



